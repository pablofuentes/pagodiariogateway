package com.pagodiario.gateway.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "security", fallback = SecurityServiceFallback.class)
public interface SecurityService {

    @GetMapping("/oauth/check_token")
    Map checkToken(@RequestHeader String Authorization, @RequestParam String token);

}



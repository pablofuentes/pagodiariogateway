package com.pagodiario.gateway.remote;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class SecurityServiceFallback implements SecurityService {

    @Override
    public Map checkToken(String authorization, String token) {
        log.info("No valid Token");
        return null;
    }
}

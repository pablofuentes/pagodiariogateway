package com.pagodiario.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.pagodiario.gateway.remote.SecurityService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

@Slf4j
public class TokenFilter extends ZuulFilter {

    @Autowired
    private SecurityService securityService;

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        String requestURI = RequestContext.getCurrentContext().getRequest().getRequestURI();
        Boolean shouldFilter = !requestURI.contains("/oauth/token") &&
                !requestURI.contains("/oauth/check_token") &&
                !requestURI.contains("/oauth/token/refresh") &&
                !requestURI.contains("index.html") &&
                !requestURI.contains(".js") &&
                !requestURI.contains(".css") &&
                !requestURI.contains(".img");
        log.info("Request need be filtered : " + shouldFilter);
        return shouldFilter;
    }

    @Override
    public Object run() {

        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));

        Enumeration<String> headerNames = request.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                String header = headerNames.nextElement();
                //log.info("Header: " + header + " Value: " + request.getHeader(header));
            }
        }

        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("Checking if token is present : " + !StringUtils.isEmpty(authorization));
        if (!StringUtils.isEmpty(authorization)) {
            log.info("OK token is present");
            log.info("Checking if token is valid : " + authorization);
            Map secure = securityService.checkToken("Basic c29sYXJGcm9udGVuZEFwcDpzZWNyZXQ=", authorization);
            log.info("Response " + secure);
            if (!ObjectUtils.isEmpty(secure)) {
                return null;
            }
        }
        redirectLogin(ctx);
        return null;
    }

    private void redirectLogin(RequestContext ctx) {
        try {
            ctx.setSendZuulResponse(false);
            ctx.put(FilterConstants.FORWARD_TO_KEY, "/ui/index.html");
            ctx.setResponseStatusCode(HttpStatus.SC_TEMPORARY_REDIRECT);
            ctx.getResponse().sendRedirect("/ui/index.html");
        } catch (Exception e) {
            log.error(e.toString());
        }
    }
}
